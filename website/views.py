from django.http import HttpResponse
from django.shortcuts import render


def index(request):
	context = {
		'judul':'Selamat Datang',
		'subjudul':'Home',
		'banner':'img/banner.jpg',
		'nav':[
			['/' ,'Home' ],
			['/blog' ,'Blog' ],
			['/about' ,'About' ],
			['/registrasi','Registrasi'],

		]
	}
	return render(request,'index.html', context)
