
from django.conf.urls import url, include
from django.contrib import admin
from . import views


urlpatterns = [
	url(r'^jet/',include('jet.urls','jet')),
	url(r'^jet/dashboard/',include('jet.dashboard.urls','jet-dashboard')),	
    url(r'^registrasi/',include('registrasi.urls',namespace='registrasi')),
    url(r'^admin/', admin.site.urls),
    url(r'^blog/',include('blog.urls')),
    url(r'^about/',include('about.urls')),
    url(r'^$',views.index),
]
